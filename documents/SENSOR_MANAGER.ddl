-- *********************************************
-- * Standard SQL generation                   
-- *--------------------------------------------
-- * DB-MAIN version: 9.2.0_32              
-- * Generator date: Apr 23 2015              
-- * Generation date: Thu Jul  5 23:01:56 2018 
-- * LUN file: /Users/mattiavandi/Development/Courses/ASW/sensormanagerproject/documents/SENSOR_MANAGER.lun 
-- * Schema: SCHEMA/SQL 
-- ********************************************* 

-- Tables Section
-- _____________ 

create table detections (
     id int not null AUTO_INCREMENT,
     sensor_room_id int not null,
     constraint ID_detections_ID primary key (id));

create table detection_values (
     detection_id int not null,
     value float not null);

create table rooms (
     id int not null AUTO_INCREMENT,
     name varchar(15) not null,
     constraint ID_rooms_ID primary key (id));

create table sensor_room (
     id int not null AUTO_INCREMENT,
     room_id int not null,
     sensor_id int not null,
     constraint ID_sensor_room_ID primary key (id));

create table sensor_types (
     id int not null AUTO_INCREMENT,
     name varchar(15) not null,
     constraint ID_sensor_types_ID primary key (id));

create table sensors (
     id int not null AUTO_INCREMENT,
     name varchar(15) not null,
     sensor_type_id int not null,
     constraint ID_sensors_ID primary key (id));

-- Constraints Section
-- ___________________ 

alter table detections add constraint ID_detections_CHK
     check(exists(select * from detection_values
                  where detection_values.detection_id = id));

alter table detections add constraint REF_detec_senso_FK
     foreign key (sensor_room_id)
     references sensor_room;

alter table detection_values add constraint EQU_detec_detec_FK
     foreign key (detection_id)
     references detections;

alter table sensor_room add constraint REF_senso_rooms_FK
     foreign key (room_id)
     references rooms;

alter table sensor_room add constraint EQU_senso_senso_FK
     foreign key (sensor_id)
     references sensors;

alter table sensors add constraint ID_sensors_CHK
     check(exists(select * from sensor_room
                  where sensor_room.sensor_id = id)); 

alter table sensors add constraint REF_senso_senso_FK
     foreign key (sensor_type_id)
     references sensor_types;


-- Index Section
-- _____________ 

create unique index ID_detections_IND
     on detections (id);

create index REF_detec_senso_IND
     on detections (sensor_room_id);

create index EQU_detec_detec_IND
     on detection_values (detection_id);

create unique index ID_rooms_IND
     on rooms (id);

create unique index ID_sensor_room_IND
     on sensor_room (id);

create index REF_senso_rooms_IND
     on sensor_room (room_id);

create index EQU_senso_senso_IND
     on sensor_room (sensor_id);

create unique index ID_sensor_types_IND
     on sensor_types (id);

create unique index ID_sensors_IND
     on sensors (id);

create index REF_senso_senso_IND
     on sensors (sensor_type_id);
