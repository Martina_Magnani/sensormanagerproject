export class Room {
    constructor(
        public name: string,
        public _id?: string
    ) { }
}

export function clone(room: Room): Room {
    return {
        _id: room._id,
        name: room.name
    };
}
