import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { DetectionsComponent } from './detections/detections.component';
import { RoomsComponent } from './rooms/rooms.component';
import { SensorsComponent } from './sensors/sensors.component';
import { AppRoutingModule } from './app-routing.module';
import { IndexComponent } from './index/index.component';
import { UpdateRoomFormComponent } from './update-room-form/update-room-form.component';
import { HttpClient } from '../../node_modules/@angular/common/http';
import { SensorManagerAdminService } from './sensor-manager-admin.service';
import { InsertRoomFormComponent } from './insert-room-form/insert-room-form.component';
import { RoomDeletionFormComponent } from './room-deletion-form/room-deletion-form.component';
import { RoomSelectionFormComponent } from './room-selection-form/room-selection-form.component';
import { DetectionInsertionFormComponent } from './detection-insertion-form/detection-insertion-form.component';
import { DetectionSelectionFormComponent } from './detection-selection-form/detection-selection-form.component';
import { DetectionDeletionFormComponent } from './detection-deletion-form/detection-deletion-form.component';
import { SensorSelectionFormComponent } from './sensor-selection-form/sensor-selection-form.component';
import { SensorInsertionFormComponent } from './sensor-insertion-form/sensor-insertion-form.component';
import { SensorUpdateFormComponent } from './sensor-update-form/sensor-update-form.component';
import { SensorDeletionFormComponent } from './sensor-deletion-form/sensor-deletion-form.component';
import { SensorTypeSelectionComponent } from './sensor-type-selection/sensor-type-selection.component';
import { EventBusService } from './event-bus.service';
import { ServerInterceptor } from './server-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    DetectionsComponent,
    RoomsComponent,
    SensorsComponent,
    IndexComponent,
    UpdateRoomFormComponent,
    InsertRoomFormComponent,
    RoomDeletionFormComponent,
    RoomSelectionFormComponent,
    DetectionInsertionFormComponent,
    DetectionSelectionFormComponent,
    DetectionDeletionFormComponent,
    SensorSelectionFormComponent,
    SensorInsertionFormComponent,
    SensorUpdateFormComponent,
    SensorDeletionFormComponent,
    SensorTypeSelectionComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [HttpClient, SensorManagerAdminService, EventBusService,
    { provide: HTTP_INTERCEPTORS, useClass: ServerInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
