export class SensorType {
    constructor(
        public _id: string,
        public name: string
    ) { }
}
