import { TestBed, inject } from '@angular/core/testing';

import { SensorManagerAdminService } from './sensor-manager-admin.service';

describe('SensorManagerAdminService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SensorManagerAdminService]
    });
  });

  it('should be created', inject([SensorManagerAdminService], (service: SensorManagerAdminService) => {
    expect(service).toBeTruthy();
  }));
});
