import { Component, OnInit, ViewChild } from '@angular/core';
import { SensorManagerAdminService } from '../sensor-manager-admin.service';
import { RoomSelectionFormComponent } from '../room-selection-form/room-selection-form.component';
import { Room } from '../room';

@Component({
  selector: 'update-room-form',
  templateUrl: './update-room-form.component.html',
  styleUrls: ['./update-room-form.component.css']
})
export class UpdateRoomFormComponent implements OnInit {

  submitted: boolean;
  model: Room;

  @ViewChild(RoomSelectionFormComponent)
  roomSelectionComponent: RoomSelectionFormComponent;

  constructor(private service: SensorManagerAdminService) {
    this.submitted = false;
    this.model = new Room('');
  }

  ngOnInit() { }

  onSubmit() {
    this.submitted = true;
    this.updateRoom();
  }

  updateRoom() {
    this.roomSelectionComponent.model.name = this.model.name
    this.service.updateRoom(this.roomSelectionComponent.model);
  }

}
