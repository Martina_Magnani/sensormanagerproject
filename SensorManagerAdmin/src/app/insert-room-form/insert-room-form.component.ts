import { Component, OnInit } from '@angular/core';
import { Room } from '../room';
import { SensorManagerAdminService } from '../sensor-manager-admin.service';

@Component({
  selector: 'insert-room-form',
  templateUrl: './insert-room-form.component.html',
  styleUrls: ['./insert-room-form.component.css']
})
export class InsertRoomFormComponent implements OnInit {

  model: Room;
  roomSavedName: string;
  submitted = false;

  constructor(private service: SensorManagerAdminService) {
    this.model = new Room('');
  }

  ngOnInit() { }

  onSubmit() {
    this.submitted = true;
    this.roomSavedName = this.model.name;
    this.service.addRoom(this.model);
  }

}
