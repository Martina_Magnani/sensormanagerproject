import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { DetectionsComponent } from './detections/detections.component';
import { RoomsComponent } from './rooms/rooms.component';
import { SensorsComponent } from './sensors/sensors.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'detections', component: DetectionsComponent },
  { path: 'rooms', component: RoomsComponent },
  { path: 'sensors', component: SensorsComponent }
];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
