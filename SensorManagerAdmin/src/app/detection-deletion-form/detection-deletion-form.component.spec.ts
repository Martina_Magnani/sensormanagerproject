import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetectionDeletionFormComponent } from './detection-deletion-form.component';

describe('DetectionDeletionFormComponent', () => {
  let component: DetectionDeletionFormComponent;
  let fixture: ComponentFixture<DetectionDeletionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetectionDeletionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetectionDeletionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
