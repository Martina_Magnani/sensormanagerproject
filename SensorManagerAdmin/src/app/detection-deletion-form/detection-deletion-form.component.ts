import { Component, OnInit, ViewChild } from '@angular/core';
import { Detection } from '../detection';
import { DetectionSelectionFormComponent } from '../detection-selection-form/detection-selection-form.component';
import { SensorManagerAdminService } from '../sensor-manager-admin.service';
import { remove } from 'lodash';

@Component({
  selector: 'detection-deletion-form',
  templateUrl: './detection-deletion-form.component.html',
  styleUrls: ['./detection-deletion-form.component.css']
})
export class DetectionDeletionFormComponent implements OnInit {

  submitted: boolean;

  @ViewChild(DetectionSelectionFormComponent)
  detectionSelectionComponent: DetectionSelectionFormComponent;

  constructor(private service: SensorManagerAdminService) { }

  ngOnInit() { }

  onSubmit() {
    this.submitted = true;
    this.deleteDetection();
  }

  deleteDetection() {
    this.service.deleteDetection(this.detectionSelectionComponent.model);
    remove(this.detectionSelectionComponent.detections, detection => detection._id === this.detectionSelectionComponent.model._id);
  }

  onReset() {
    this.detectionSelectionComponent.model = new Detection([]);
  }

}
