import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorTypeSelectionComponent } from './sensor-type-selection.component';

describe('SensorTypeSelectionComponent', () => {
  let component: SensorTypeSelectionComponent;
  let fixture: ComponentFixture<SensorTypeSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SensorTypeSelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorTypeSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
