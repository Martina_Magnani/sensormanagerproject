import { Component, OnInit } from '@angular/core';
import { SensorType } from '../sensor-type';
import { SensorManagerAdminService } from '../sensor-manager-admin.service';
import { remove } from 'lodash';

@Component({
  selector: 'sensor-type-selection',
  templateUrl: './sensor-type-selection.component.html',
  styleUrls: ['./sensor-type-selection.component.css']
})
export class SensorTypeSelectionComponent implements OnInit {

  sensorTypes: SensorType[];
  model: SensorType;

  constructor(private service: SensorManagerAdminService) {
    this.model = new SensorType('', '');
  }

  ngOnInit() {
    this.service.querySensorTypes().subscribe(sensorTypes => {
      this.sensorTypes = sensorTypes;
    });

  }

  selectSensorType(sensorType: SensorType) {
    this.model = sensorType;
  }

}
