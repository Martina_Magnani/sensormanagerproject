import { Injectable } from '@angular/core';

import { Detection } from './detection';
import { Sensor } from './sensor';
import { Room } from './room';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { SensorType } from './sensor-type';
import { EventBusService } from './event-bus.service';
import { ObjectId } from './object-id';

@Injectable({
  providedIn: 'root'
})
export class SensorManagerAdminService {

  /* Server REST API Paths*/
  private static SENSORS = '/api/sensors';
  private static SENSOR_TYPES = '/api/sensor-types';
  private static ROOMS = '/api/rooms';
  private static DETECTIONS = '/api/detections';

  /* Event Channel Info */
  private static EVENT_CHANNEL_ADDRESS = 'http://sheltered-waters-98453.herokuapp.com/api/events';
  private static STORED_DETECTION = 'detections.stored';
  private static DELETED_DETECTION = 'detections.destroyed';
  private static STORED_SENSOR = 'sensors.stored';
  private static UPDATED_SENSOR = 'sensors.updated';
  private static DELETED_SENSOR = 'sensors.destroyed';
  private static STORED_ROOM = 'rooms.stored';
  private static UPDATED_ROOM = 'rooms.updated';
  private static DELETED_ROOM = 'rooms.destroyed';


  /* RxJs Streams */
  private storedDetection: Subject<Detection>;
  private deletedDetection: Subject<ObjectId>;
  private storedRoom: Subject<Room>;
  private updatedRoom: Subject<Room>;
  private deletedRoom: Subject<ObjectId>;
  private updatedSensor: Subject<Sensor>;
  private storedSensor: Subject<Sensor>;
  private deletedSensor: Subject<ObjectId>;


  constructor(private http: HttpClient, eventBus: EventBusService) {
    this.storedDetection = new Subject<Detection>();
    this.deletedDetection = new Subject<ObjectId>();
    this.storedRoom = new Subject<Room>();
    this.updatedRoom = new Subject<Room>();
    this.deletedRoom = new Subject<ObjectId>();
    this.storedSensor = new Subject<Sensor>();
    this.deletedSensor = new Subject<ObjectId>();
    this.updatedSensor = new Subject<Sensor>();


    /* ------------ Vertx Event Bus Management ------------*/
    /* Vertx EventBus Connection */
    eventBus.connect(SensorManagerAdminService.EVENT_CHANNEL_ADDRESS);

    /* Detection Subscriptions */
    eventBus.registerHandler(SensorManagerAdminService.STORED_DETECTION, (err, msg) => {
      this.storedDetection.next(msg.body);
    });

    eventBus.registerHandler(SensorManagerAdminService.DELETED_DETECTION, (err, msg) => {
      this.deletedDetection.next(msg.body);
    });

    /* Sensors Subscriptions */
    eventBus.registerHandler(SensorManagerAdminService.UPDATED_SENSOR, (err, msg) => {
      this.updatedSensor.next(msg.body);
    });

    eventBus.registerHandler(SensorManagerAdminService.STORED_SENSOR, (err, msg) => {
      this.storedSensor.next(msg.body);
    });

    eventBus.registerHandler(SensorManagerAdminService.DELETED_SENSOR, (err, msg) => {
      this.deletedSensor.next(msg.body);
    });

    /* Rooms Subscriptions */
    eventBus.registerHandler(SensorManagerAdminService.STORED_ROOM, (err, msg) => {
      this.storedRoom.next(msg.body);
    });

    eventBus.registerHandler(SensorManagerAdminService.UPDATED_ROOM, (err, msg) => {
      this.updatedRoom.next(msg.body);
    });

    eventBus.registerHandler(SensorManagerAdminService.DELETED_ROOM, (err, msg) => {
      this.deletedRoom.next(msg.body);
    });

  }

  /* --------------- REST API Methods --------------- */
  /* Sensors */
  querySensors(): Observable<Sensor[]> {
    return this.http.get<Sensor[]>(SensorManagerAdminService.SENSORS);
  }

  querySensor(sensorId: string): Observable<Sensor> {
    return this.http.get<Sensor>(SensorManagerAdminService.SENSORS + '/' + sensorId);
  }

  addSensor(sensor: Sensor) {
    return this.http.post<Sensor>(SensorManagerAdminService.SENSORS, sensor).subscribe();
  }

  updateSensor(sensor: Sensor) {
    return this.http.put<Sensor>(SensorManagerAdminService.SENSORS + '/' + sensor._id, sensor).subscribe();
  }

  deleteSensor(sensor: Sensor, onComplete: () => any) {
    return this.http.delete<Sensor>(SensorManagerAdminService.SENSORS + '/' + sensor._id).subscribe(onComplete);
  }

  /* Sensor Types */
  querySensorTypes(): Observable<SensorType[]> {
    return this.http.get<SensorType[]>(SensorManagerAdminService.SENSOR_TYPES);
  }

  /* Rooms */
  queryRooms(): Observable<Room[]> {
    return this.http.get<Room[]>(SensorManagerAdminService.ROOMS);
  }

  queryRoom(room: Room): Observable<Room> {
    return this.http.get<Room>(SensorManagerAdminService.ROOMS + '/' + room._id);
  }

  addRoom(room: Room) {
    return this.http.post<Room>(SensorManagerAdminService.ROOMS, room).subscribe();
  }

  updateRoom(room: Room) {
    return this.http.put<Room>(SensorManagerAdminService.ROOMS + '/' + room._id, room).subscribe();
  }

  deleteRoom(room: Room, onComplete: () => any) {
    return this.http.delete<Room>(SensorManagerAdminService.ROOMS + '/' + room._id).subscribe(onComplete);
  }

  /* Detections */
  queryDetections(): Observable<Detection[]> {
    return this.http.get<Detection[]>(SensorManagerAdminService.DETECTIONS);
  }

  addDetection(detection: Detection) {
    return this.http.post(SensorManagerAdminService.DETECTIONS, detection).subscribe();
  }

  deleteDetection(detection: Detection) {
    return this.http.delete(SensorManagerAdminService.DETECTIONS + '/' + detection._id).subscribe();
  }

  /* Stream Observables Getters */
  /* Detections */
  onStoredDetection(): Observable<Detection> {
    return this.storedDetection.asObservable();
  }

  onDeletedDetection(): Observable<ObjectId> {
    return this.deletedDetection.asObservable();
  }

  /* Rooms */
  onStoredRoom(): Observable<Room> {
    return this.storedRoom.asObservable();
  }

  onUpdatedRoom(): Observable<Room> {
    return this.updatedRoom.asObservable();
  }

  onDeletedRoom(): Observable<ObjectId> {
    return this.deletedRoom.asObservable();
  }

  /* Sensors */
  onStoredSensor(): Observable<Sensor> {
    return this.storedSensor.asObservable();
  }

  onUpdatedSensor(): Observable<Sensor> {
    return this.updatedSensor.asObservable();
  }

  onDeletedSensor(): Observable<ObjectId> {
    return this.deletedSensor.asObservable();
  }

}
