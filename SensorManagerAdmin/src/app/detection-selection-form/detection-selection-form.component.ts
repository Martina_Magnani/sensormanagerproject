import { Component, OnInit } from '@angular/core';
import { Detection } from '../detection';
import { SensorManagerAdminService } from '../sensor-manager-admin.service';
import { Sensor } from '../sensor';
import { remove } from 'lodash';

@Component({
  selector: 'detection-selection',
  templateUrl: './detection-selection-form.component.html',
  styleUrls: ['./detection-selection-form.component.css']
})
export class DetectionSelectionFormComponent implements OnInit {

  detections: Detection[];
  model: Detection;

  constructor(private service: SensorManagerAdminService) {
    this.model = new Detection([], '', new Sensor('', 0, 0));
    this.detections = [];
  }

  ngOnInit() {
    this.service.queryDetections().subscribe(detections => {
      this.detections = detections;
    });

    this.service.onStoredDetection()
      .subscribe(detection => this.detections.unshift(detection));

    this.service.onDeletedDetection()
      .subscribe(detectionId => remove(this.detections, det => det._id === detectionId._id));
  }

  selectDetection(detection: Detection) {
    this.model = detection;
  }

}
