import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetectionSelectionFormComponent } from './detection-selection-form.component';

describe('DetectionSelectionFormComponent', () => {
  let component: DetectionSelectionFormComponent;
  let fixture: ComponentFixture<DetectionSelectionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetectionSelectionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetectionSelectionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
