import { Component, OnInit } from '@angular/core';
import { Room } from '../room';
import { SensorManagerAdminService } from '../sensor-manager-admin.service';
import { remove } from 'lodash';

@Component({
  selector: 'room-selection',
  templateUrl: './room-selection-form.component.html',
  styleUrls: ['./room-selection-form.component.css']
})
export class RoomSelectionFormComponent implements OnInit {

  rooms: Room[];
  model: Room

  constructor(private service: SensorManagerAdminService) {
    this.rooms = [];
    this.model = new Room('', '');
  }

  ngOnInit() {
    this.service.queryRooms().subscribe(rooms => {
      this.rooms = rooms;
    });

    this.service.onStoredRoom().subscribe(room => this.rooms.unshift(room));

    this.service.onUpdatedRoom().subscribe(roomUpdated => {
      remove(this.rooms, room => room._id === roomUpdated._id);
      this.rooms.unshift(roomUpdated);
    });

    this.service.onDeletedRoom().subscribe(oid => remove(this.rooms, room => room._id === oid._id));
  }

  selectRoom(room: Room) {
    this.model = room;
  }

}
