import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomSelectionFormComponent } from './room-selection-form.component';

describe('RoomSelectionFormComponent', () => {
  let component: RoomSelectionFormComponent;
  let fixture: ComponentFixture<RoomSelectionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomSelectionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomSelectionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
