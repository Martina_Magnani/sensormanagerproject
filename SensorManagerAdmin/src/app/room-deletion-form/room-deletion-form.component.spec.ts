import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoomDeletionFormComponent } from './room-deletion-form.component';

describe('RoomDeletionFormComponent', () => {
  let component: RoomDeletionFormComponent;
  let fixture: ComponentFixture<RoomDeletionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomDeletionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoomDeletionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
