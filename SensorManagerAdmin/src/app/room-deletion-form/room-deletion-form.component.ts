import { Component, OnInit, ViewChild } from '@angular/core';
import { SensorManagerAdminService } from '../sensor-manager-admin.service';
import { RoomSelectionFormComponent } from '../room-selection-form/room-selection-form.component';
import { remove } from 'lodash';
import { Room } from '../room';

@Component({
  selector: 'deletion-room-form',
  templateUrl: './room-deletion-form.component.html',
  styleUrls: ['./room-deletion-form.component.css']
})
export class RoomDeletionFormComponent implements OnInit {

  submitted = false;

  @ViewChild(RoomSelectionFormComponent)
  roomSelectionComponent: RoomSelectionFormComponent;

  constructor(private service: SensorManagerAdminService) { }

  ngOnInit() { }

  onSubmit() {
    this.submitted = true;
    this.deleteRoom();
  }

  deleteRoom() {
    this.service.deleteRoom(this.roomSelectionComponent.model,
      () => remove(this.roomSelectionComponent.rooms, room => room._id === this.roomSelectionComponent.model._id));
  }

  onReset() {
    this.roomSelectionComponent.model = new Room('');
  }

}
