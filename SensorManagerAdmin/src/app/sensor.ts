import { Room } from "./room";
import { SensorType } from "./sensor-type";

export class Sensor {
    constructor(
        public name: string,
        public samplingPeriod?: number,
        public barcodeId?: number,
        public _id?: string,
        public room?: Room,
        public type?: SensorType,
        public roomId?: string,
        public typeId?: string
    ) { }
}
