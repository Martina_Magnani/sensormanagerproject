import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetectionInsertionFormComponent } from './detection-insertion-form.component';

describe('DetectionInsertionFormComponent', () => {
  let component: DetectionInsertionFormComponent;
  let fixture: ComponentFixture<DetectionInsertionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetectionInsertionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetectionInsertionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
