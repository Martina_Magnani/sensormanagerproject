import { Component, OnInit, ViewChild } from '@angular/core';
import { SensorSelectionFormComponent } from '../sensor-selection-form/sensor-selection-form.component';
import { SensorManagerAdminService } from '../sensor-manager-admin.service';
import { Detection } from '../detection';
import { Sensor } from '../sensor';

@Component({
  selector: 'detection-insertion-form',
  templateUrl: './detection-insertion-form.component.html',
  styleUrls: ['./detection-insertion-form.component.css']
})
export class DetectionInsertionFormComponent implements OnInit {

  submitted: boolean;
  lastSavedValues: number[];
  model: Detection;

  @ViewChild(SensorSelectionFormComponent)
  sensorSelectionComponent: SensorSelectionFormComponent;

  constructor(private service: SensorManagerAdminService) {
    this.submitted = false;
    this.model = new Detection([]);
    this.lastSavedValues = [];
  }

  ngOnInit() { }

  onSubmit() {
    this.submitted = true;
    this.insertDetection();
  }

  onReset() {
    this.sensorSelectionComponent.model = new Sensor('', 0, undefined);
  }

  insertDetection() {
    this.model.sensorId = this.sensorSelectionComponent.model._id;
    this.copyValues();
    this.service.addDetection(this.model);
  }

  copyValues() {
    this.model.values.forEach((value, index) => this.lastSavedValues[index] = value);
  }

}
