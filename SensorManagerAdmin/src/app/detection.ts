import { Sensor } from "./sensor";

export class Detection {
    constructor(
        public values: number[],
        public sensorId?: string,
        public sensor?: Sensor,
        public _id?: string
    ) { }
}
