import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorSelectionFormComponent } from './sensor-selection-form.component';

describe('SensorSelectionFormComponent', () => {
  let component: SensorSelectionFormComponent;
  let fixture: ComponentFixture<SensorSelectionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SensorSelectionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorSelectionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
