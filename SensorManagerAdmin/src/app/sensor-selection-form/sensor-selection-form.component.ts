import { Component, OnInit } from '@angular/core';
import { Sensor } from '../sensor';
import { SensorManagerAdminService } from '../sensor-manager-admin.service';
import { remove } from 'lodash';

@Component({
  selector: 'sensor-selection',
  templateUrl: './sensor-selection-form.component.html',
  styleUrls: ['./sensor-selection-form.component.css']
})
export class SensorSelectionFormComponent implements OnInit {

  sensors: Sensor[];
  model: Sensor;

  constructor(private service: SensorManagerAdminService) {
    this.model = new Sensor('', 0, undefined);
  }

  ngOnInit() {
    this.service.querySensors().subscribe(sensors => {
      this.sensors = sensors;
    });

    this.service.onStoredSensor().subscribe(sensor => this.sensors.unshift(sensor));

    this.service.onUpdatedSensor().subscribe(sensorUpdated => {
      remove(this.sensors, sensor => sensor._id === sensorUpdated._id);
      this.sensors.unshift(sensorUpdated);
    });

    this.service.onDeletedSensor().subscribe(oid => remove(this.sensors, sensor => sensor._id === oid._id));
  }

  selectSensor(sensor: Sensor) {
    this.model = sensor;
  }

}
