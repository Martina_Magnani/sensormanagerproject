import { Component, OnInit, ViewChild } from '@angular/core';
import { Sensor } from '../sensor';
import { SensorSelectionFormComponent } from '../sensor-selection-form/sensor-selection-form.component';
import { RoomSelectionFormComponent } from '../room-selection-form/room-selection-form.component';
import { SensorManagerAdminService } from '../sensor-manager-admin.service';
import { Room } from '../room';

@Component({
  selector: 'sensor-update-form',
  templateUrl: './sensor-update-form.component.html',
  styleUrls: ['./sensor-update-form.component.css']
})
export class SensorUpdateFormComponent implements OnInit {

  submitted: boolean;
  model: Sensor;

  @ViewChild(SensorSelectionFormComponent)
  sensorSelectionComponent: SensorSelectionFormComponent;

  constructor(private service: SensorManagerAdminService) {
    this.submitted = false;
    this.model = new Sensor('', 0, undefined);
  }

  ngOnInit() { }

  onContinue() {
    window.location.reload();
  }

  onReset() {
    this.sensorSelectionComponent.model = new Sensor('', 0, undefined);
  }

  onSubmit() {
    this.submitted = true;
    this.updateSensor();
  }

  updateSensor() {
    this.sensorSelectionComponent.model.name = this.model.name;
    this.service.updateSensor(this.sensorSelectionComponent.model);
  }

}
