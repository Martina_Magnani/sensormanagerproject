import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorInsertionFormComponent } from './sensor-insertion-form.component';

describe('SensorInsertionFormComponent', () => {
  let component: SensorInsertionFormComponent;
  let fixture: ComponentFixture<SensorInsertionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SensorInsertionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorInsertionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
