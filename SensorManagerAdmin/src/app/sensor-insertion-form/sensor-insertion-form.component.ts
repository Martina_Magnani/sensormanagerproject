import { Component, OnInit, ViewChild } from '@angular/core';
import { Sensor } from '../sensor';
import { SensorTypeSelectionComponent } from '../sensor-type-selection/sensor-type-selection.component';
import { RoomSelectionFormComponent } from '../room-selection-form/room-selection-form.component';
import { SensorManagerAdminService } from '../sensor-manager-admin.service';
import { SensorType } from '../sensor-type';

@Component({
  selector: 'sensor-insertion-form',
  templateUrl: './sensor-insertion-form.component.html',
  styleUrls: ['./sensor-insertion-form.component.css']
})
export class SensorInsertionFormComponent implements OnInit {

  submitted: boolean;
  lastSavedName: string;
  lastSavedBarcodeId: number;
  lastSavedSamplingPeriod: number;
  model: Sensor;

  @ViewChild(RoomSelectionFormComponent)
  roomSelectionComponent: RoomSelectionFormComponent;

  @ViewChild(SensorTypeSelectionComponent)
  sensorTypeSelectionComponent: SensorTypeSelectionComponent;

  constructor(private service: SensorManagerAdminService) {
    this.submitted = false;
    this.model = new Sensor('', undefined, undefined);
  }

  ngOnInit() { }

  onSubmit() {
    this.submitted = true;
    this.insertSensor();
  }

  onReset() {
    this.sensorTypeSelectionComponent.model = new SensorType('', '');
  }

  onContinue() {
    window.location.reload();
  }

  insertSensor() {
    this.model.roomId = this.roomSelectionComponent.model._id;
    this.model.typeId = this.sensorTypeSelectionComponent.model._id;
    this.copyValues();
    this.service.addSensor(this.model);
  }

  copyValues() {
    this.lastSavedSamplingPeriod = this.model.samplingPeriod;
    this.lastSavedName = this.model.name;
    this.lastSavedBarcodeId = this.model.barcodeId;
  }

}
