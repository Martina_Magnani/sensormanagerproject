import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SensorDeletionFormComponent } from './sensor-deletion-form.component';

describe('SensorDeletionFormComponent', () => {
  let component: SensorDeletionFormComponent;
  let fixture: ComponentFixture<SensorDeletionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SensorDeletionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SensorDeletionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
