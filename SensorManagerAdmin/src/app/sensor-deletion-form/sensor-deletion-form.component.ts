import { Component, OnInit, ViewChild } from '@angular/core';
import { SensorSelectionFormComponent } from '../sensor-selection-form/sensor-selection-form.component';
import { SensorManagerAdminService } from '../sensor-manager-admin.service';
import { Sensor } from '../sensor';
import { remove } from 'lodash';

@Component({
  selector: 'sensor-deletion-form',
  templateUrl: './sensor-deletion-form.component.html',
  styleUrls: ['./sensor-deletion-form.component.css']
})
export class SensorDeletionFormComponent implements OnInit {

  submitted: boolean;

  @ViewChild(SensorSelectionFormComponent)
  sensorSelectionComponent: SensorSelectionFormComponent;

  constructor(private service: SensorManagerAdminService) {
    this.submitted = false;
  }

  ngOnInit() { }

  onContinue() {
    window.location.reload();
  }

  onSubmit() {
    this.service.deleteSensor(this.sensorSelectionComponent.model,
      () => {
        this.submitted = true;
        remove(this.sensorSelectionComponent.sensors, sensor => sensor._id === this.sensorSelectionComponent.model._id)
      });
  }

  onReset() {
    this.sensorSelectionComponent.model = new Sensor('', 0, undefined);
  }

}
