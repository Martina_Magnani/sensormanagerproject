#!/bin/bash
if [ ! -z ${PORT+x} ]; then
    port=$PORT
elif [ "$#" -gt 0 ]; then
    port=$1
else
    port="8080"
fi
java -jar build/libs/sensor-manager-server-1.0.jar $port
