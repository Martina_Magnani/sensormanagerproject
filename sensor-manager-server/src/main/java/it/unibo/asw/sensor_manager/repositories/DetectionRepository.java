package it.unibo.asw.sensor_manager.repositories;

import it.unibo.asw.sensor_manager.domain.Detection;
import org.bson.types.ObjectId;

public interface DetectionRepository extends Repository<Detection, ObjectId> {
}
