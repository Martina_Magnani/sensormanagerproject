package it.unibo.asw.sensor_manager.repositories;

import io.reactivex.Single;
import it.unibo.asw.sensor_manager.domain.SensorType;
import org.bson.types.ObjectId;

public interface SensorTypeRepository extends Repository<SensorType, ObjectId> {

    @Override
    Single<SensorType> findById(ObjectId objectId);

}
