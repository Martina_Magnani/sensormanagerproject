package it.unibo.asw.sensor_manager.domain;

import io.vertx.core.json.JsonObject;
import it.unibo.asw.sensor_manager.util.JsonHelper;
import it.unibo.asw.sensor_manager.util.ObjectIdHelper;
import org.bson.types.ObjectId;

import java.util.Objects;

public class SensorType {

    private ObjectId id;
    private String name;

    public static SensorType fromJson(JsonObject json) {
        final SensorType sensorType = new SensorType();

        ObjectIdHelper.fromJson(json).ifPresent(sensorType::setId);

        JsonHelper.ifPresentString(json, "name", sensorType::setName);

        return sensorType;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SensorType)) return false;
        SensorType that = (SensorType) o;
        return Objects.equals(getId(), that.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getName());
    }

    public JsonObject toJson() {
        final JsonObject json = new JsonObject();

        if (id != null) {
            json.mergeIn(ObjectIdHelper.toJson(getId()));
        }

        if (name != null) {
            json.put("name", getName());
        }

        return json;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

}
