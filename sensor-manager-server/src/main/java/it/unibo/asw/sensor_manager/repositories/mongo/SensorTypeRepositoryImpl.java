package it.unibo.asw.sensor_manager.repositories.mongo;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.mongo.MongoClient;
import it.unibo.asw.sensor_manager.domain.SensorType;
import it.unibo.asw.sensor_manager.repositories.SensorTypeRepository;
import it.unibo.asw.sensor_manager.util.ObjectIdHelper;
import org.bson.types.ObjectId;

import static it.unibo.asw.sensor_manager.util.Utils.TODO;

public class SensorTypeRepositoryImpl implements SensorTypeRepository {

    private final MongoClient client;

    public SensorTypeRepositoryImpl(MongoClient client) {
        this.client = client;
    }

    @Override
    public Observable<SensorType> findAll() {
        return client.rxFind(Collections.SENSOR_TYPES, new JsonObject())
                .toObservable()
                .flatMapIterable(x -> x)
                .map(SensorType::fromJson);
    }

    @Override
    public Single<SensorType> findById(ObjectId id) {
        final JsonObject query = ObjectIdHelper.toJson(id);

        return client.rxFindOne(Collections.SENSOR_TYPES, query, null)
                .map(SensorType::fromJson);
    }

    @Override
    public Single<ObjectId> save(SensorType sensorType) {
        return TODO("This method should not be called");
    }

    @Override
    public Completable deleteById(ObjectId objectId) {
        return TODO("This method should not be called");
    }

}
