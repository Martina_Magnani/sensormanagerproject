package it.unibo.asw.sensor_manager.repositories;

import it.unibo.asw.sensor_manager.domain.Room;
import org.bson.types.ObjectId;

public interface RoomRepository extends Repository<Room, ObjectId> {
}
