package it.unibo.asw.sensor_manager;

import io.vertx.core.DeploymentOptions;
import io.vertx.core.VertxOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.Vertx;
import io.vertx.reactivex.core.eventbus.EventBus;
import io.vertx.reactivex.ext.mongo.MongoClient;
import it.unibo.asw.sensor_manager.api.Api;
import it.unibo.asw.sensor_manager.api.ApiConfig;
import it.unibo.asw.sensor_manager.api.handlers.impl.DetectionsHandlerImpl;
import it.unibo.asw.sensor_manager.api.handlers.impl.RoomsHandlerImpl;
import it.unibo.asw.sensor_manager.api.handlers.impl.SensorTypesHandlerImpl;
import it.unibo.asw.sensor_manager.api.handlers.impl.SensorsHandlerImpl;
import it.unibo.asw.sensor_manager.repositories.DetectionRepository;
import it.unibo.asw.sensor_manager.repositories.RoomRepository;
import it.unibo.asw.sensor_manager.repositories.SensorRepository;
import it.unibo.asw.sensor_manager.repositories.SensorTypeRepository;
import it.unibo.asw.sensor_manager.repositories.mongo.DetectionRepositoryImpl;
import it.unibo.asw.sensor_manager.repositories.mongo.RoomRepositoryImpl;
import it.unibo.asw.sensor_manager.repositories.mongo.SensorRepositoryImpl;
import it.unibo.asw.sensor_manager.repositories.mongo.SensorTypeRepositoryImpl;

import java.io.IOException;
import java.net.InetAddress;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Collectors;

public class Main {

    public static void main(String... args) throws Exception {
        final String host = InetAddress.getLocalHost().getHostAddress();
        final int port = Integer.parseInt(args[0]);

        final JsonObject config = config("mlab_config.json");

        Vertx.clusteredVertx(new VertxOptions(), ar -> {
            if (ar.succeeded()) {
                final Vertx vertx = ar.result();

                final EventBus eventBus = vertx.eventBus();

                final MongoClient mongoClient = MongoClient.createShared(vertx, config);

                final DetectionRepository detectionRepository = new DetectionRepositoryImpl(mongoClient);
                final RoomRepository roomRepository = new RoomRepositoryImpl(mongoClient);
                final SensorRepository sensorRepository = new SensorRepositoryImpl(mongoClient);
                final SensorTypeRepository sensorTypeRepository = new SensorTypeRepositoryImpl(mongoClient);

                ApiConfig apiConfig = ApiConfig.builder()
                        .setDetectionsHandler(new DetectionsHandlerImpl(detectionRepository, roomRepository, sensorRepository, sensorTypeRepository, eventBus))
                        .setRoomsHandler(new RoomsHandlerImpl(roomRepository, eventBus))
                        .setSensorsHandler(new SensorsHandlerImpl(roomRepository, sensorRepository, sensorTypeRepository, eventBus))
                        .setSensorTypesHandler(new SensorTypesHandlerImpl(sensorTypeRepository))
                        .setHost(host)
                        .setPort(port)
                        .build();

                vertx.getDelegate().deployVerticle(() -> new Api(apiConfig), new DeploymentOptions());
            } else {
                System.err.println(ar.cause().getMessage());
            }
        });
    }

    private static JsonObject config(final String path) throws IOException {
        return new JsonObject(Files.readAllLines(Paths.get(path)).stream()
                .collect(Collectors.joining())
                .trim());
    }

}
