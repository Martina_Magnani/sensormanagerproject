package it.unibo.asw.sensor_manager.api.handlers.impl;

import io.reactivex.disposables.Disposable;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.eventbus.EventBus;
import io.vertx.reactivex.ext.web.RoutingContext;
import it.unibo.asw.sensor_manager.C;
import it.unibo.asw.sensor_manager.api.handlers.RoomsHandler;
import it.unibo.asw.sensor_manager.domain.Room;
import it.unibo.asw.sensor_manager.repositories.RoomRepository;
import it.unibo.asw.sensor_manager.util.ObjectIdHelper;
import org.bson.types.ObjectId;

import static it.unibo.asw.sensor_manager.util.Utils.TODO;

public class RoomsHandlerImpl implements RoomsHandler {

    private final RoomRepository repository;

    private final EventBus eventBus;

    public RoomsHandlerImpl(RoomRepository repository, EventBus eventBus) {
        this.repository = repository;
        this.eventBus = eventBus;
    }

    @Override
    public Disposable index(RoutingContext context) {
        return repository.findAll()
                .map(Room::toJson)
                .toList()
                .map(JsonArray::new)
                .map(Object::toString)
                .subscribe(json -> context.response().end(json));
    }

    @Override
    public Disposable store(RoutingContext context) {
        final JsonObject body = context.getBodyAsJson();
        final Room room = Room.fromJson(body);

        return repository.save(room)
                .map(ObjectIdHelper::toJson)
                .map(room.toJson()::mergeIn)
                .subscribe(json -> {
                    eventBus.publish(C.rooms.stored, json);
                    context.response().end();
                });
    }

    @Override
    public Disposable show(RoutingContext context) {
        return TODO(getClass().getName() + ".show is not implemented");
    }

    @Override
    public Disposable update(RoutingContext context) {
        final String id = context.request().getParam("id");
        final JsonObject body = context.getBodyAsJson().put("_id", id);

        return repository.findById(ObjectIdHelper.fromString(id))
                .map(room -> {
                    room.setName(body.getString("name"));
                    return room;
                })
                .flatMap(room -> repository.save(room)
                        .map(oid -> room.toJson()))
                .subscribe(json -> {
                    eventBus.publish(C.rooms.updated, json);
                    context.response().end();
                });
    }

    @Override
    public Disposable destroy(RoutingContext context) {
        final String id = context.request().getParam("id");
        final ObjectId oid = new ObjectId(id);

        return repository.deleteById(oid)
                .subscribe(() -> {
                    final JsonObject json = new JsonObject().put("_id", id);
                    eventBus.publish(C.rooms.destroyed, json);
                    context.response().end();
                });
    }

}
