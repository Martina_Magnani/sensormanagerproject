package it.unibo.asw.sensor_manager.api.handlers;

import io.reactivex.disposables.Disposable;
import io.vertx.reactivex.ext.web.RoutingContext;

public interface SensorsHandler extends Handler {

    @Override
    Disposable store(RoutingContext context);

    @Override
    Disposable show(RoutingContext context);

    @Override
    Disposable update(RoutingContext context);

    @Override
    Disposable destroy(RoutingContext context);

}
