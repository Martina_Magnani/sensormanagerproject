package it.unibo.asw.sensor_manager.repositories.mongo;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.mongo.MongoClient;
import it.unibo.asw.sensor_manager.domain.Sensor;
import it.unibo.asw.sensor_manager.repositories.SensorRepository;
import it.unibo.asw.sensor_manager.util.ObjectIdHelper;
import org.bson.types.ObjectId;

public class SensorRepositoryImpl implements SensorRepository {

    private final MongoClient client;

    public SensorRepositoryImpl(MongoClient client) {
        this.client = client;
    }

    @Override
    public Observable<Sensor> findAll() {
        return client.rxFind(Collections.SENSORS, new JsonObject())
                .toObservable()
                .flatMapIterable(x -> x)
                .flatMap(json -> client.rxFindOne(Collections.SENSOR_TYPES, new JsonObject()
                            .put("_id", json.getString("typeId")), null)
                            .map(type -> json.put("type", type))
                            .toObservable())
                .flatMap(json -> client.rxFindOne(Collections.ROOMS,
                        new JsonObject().put("_id", json.getString("roomId")), null)
                        .map(room -> json.put("room", room))
                        .toObservable())
                .flatMap(json -> client.rxFind(Collections.DETECTIONS, new JsonObject().put("sensorId", json.getString("_id")))
                        .map(JsonArray::new)
                        .map(detections -> json.put("detections", detections))
                            .toObservable())
                .map(Sensor::fromJson);
    }

    @Override
    public Single<Sensor> findById(ObjectId id) {
        final JsonObject query = ObjectIdHelper.toJson(id);

        return client.rxFindOne(Collections.SENSORS, query, null)
                .flatMap(json -> client.rxFind(Collections.DETECTIONS, new JsonObject()
                        .put("sensorId", json.getString("_id")))
                        .map(JsonArray::new)
                        .map(detections -> json.put("detections", detections)))
                .flatMap(json -> client.rxFindOne(Collections.ROOMS,
                        new JsonObject().put("_id", json.getString("roomId")), null)
                        .map(room -> json.put("room", room)))
                .flatMap(json -> client.rxFindOne(Collections.SENSOR_TYPES,
                        new JsonObject().put("_id", json.getString("typeId")), null)
                        .map(type -> json.put("type", type)))
                .map(Sensor::fromJson);
    }

    @Override
    public Single<ObjectId> save(Sensor sensor) {
        final ObjectId id = sensor.getId();

        final JsonObject json = sensor.toJson();
        json.remove("detections");

        json.put("roomId", sensor.getRoom().getId().toString());
        json.remove("room");

        json.put("typeId", sensor.getType().getId().toString());
        json.remove("type");

        return client.rxSave(Collections.SENSORS, json)
                .map(newId -> id == null
                        ? ObjectIdHelper.fromString(newId)
                        : id);
    }

    @Override
    public Completable deleteById(ObjectId id) {
        final JsonObject query = ObjectIdHelper.toJson(id);

        return client.rxFindOneAndDelete(Collections.SENSORS, query)
                .flatMapCompletable(x -> client.rxRemoveDocuments(
                        Collections.DETECTIONS,
                        new JsonObject().put("sensorId", id.toString()))
                        .toCompletable());
    }

}
