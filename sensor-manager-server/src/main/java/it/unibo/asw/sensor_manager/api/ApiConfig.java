package it.unibo.asw.sensor_manager.api;

import it.unibo.asw.sensor_manager.api.handlers.DetectionsHandler;
import it.unibo.asw.sensor_manager.api.handlers.RoomsHandler;
import it.unibo.asw.sensor_manager.api.handlers.SensorTypesHandler;
import it.unibo.asw.sensor_manager.api.handlers.SensorsHandler;

import static java.util.Objects.requireNonNull;

public class ApiConfig {

    private final DetectionsHandler detectionsHandler;
    private final RoomsHandler roomsHandler;
    private final SensorsHandler sensorsHandler;
    private final SensorTypesHandler sensorTypesHandler;

    private final String host;
    private final int port;

    public static Builder builder() {
        return new Builder();
    }

    private ApiConfig(DetectionsHandler detectionsHandler, RoomsHandler roomsHandler, SensorsHandler sensorsHandler, SensorTypesHandler sensorTypesHandler, String host, int port) {
        this.detectionsHandler = detectionsHandler;
        this.roomsHandler = roomsHandler;
        this.sensorsHandler = sensorsHandler;
        this.sensorTypesHandler = sensorTypesHandler;
        this.host = host;
        this.port = port;
    }

    public DetectionsHandler getDetectionsHandler() {
        return detectionsHandler;
    }

    public RoomsHandler getRoomsHandler() {
        return roomsHandler;
    }

    public SensorsHandler getSensorsHandler() {
        return sensorsHandler;
    }

    public SensorTypesHandler getSensorTypesHandler() {
        return sensorTypesHandler;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public static class Builder {

        private DetectionsHandler detectionsHandler;
        private RoomsHandler roomsHandler;
        private SensorsHandler sensorsHandler;
        private SensorTypesHandler sensorTypesHandler;
        private String host;
        private int port;

        private Builder() {
        }

        public Builder setDetectionsHandler(DetectionsHandler detectionsHandler) {
            this.detectionsHandler = detectionsHandler;
            return this;
        }

        public Builder setRoomsHandler(RoomsHandler roomsHandler) {
            this.roomsHandler = roomsHandler;
            return this;
        }

        public Builder setSensorsHandler(SensorsHandler sensorsHandler) {
            this.sensorsHandler = sensorsHandler;
            return this;
        }

        public Builder setSensorTypesHandler(SensorTypesHandler sensorTypesHandler) {
            this.sensorTypesHandler = sensorTypesHandler;
            return this;
        }

        public Builder setHost(String host) {
            this.host = host;
            return this;
        }

        public Builder setPort(int port) {
            this.port = port;
            return this;
        }

        public ApiConfig build() {
            requireNonNull(detectionsHandler, "detectionsHandler is null");
            requireNonNull(roomsHandler, "roomsHandler is null");
            requireNonNull(sensorsHandler, "sensorsHandler is null");
            requireNonNull(sensorTypesHandler, "sensorTypesHandler is null");

            requireNonNull(host, "host is null");

            return new ApiConfig(detectionsHandler, roomsHandler, sensorsHandler, sensorTypesHandler, host, port);
        }
    }
}
