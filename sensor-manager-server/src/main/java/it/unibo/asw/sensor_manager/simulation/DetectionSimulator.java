package it.unibo.asw.sensor_manager.simulation;

import io.vertx.core.Context;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.core.eventbus.EventBus;
import io.vertx.reactivex.core.eventbus.Message;
import io.vertx.reactivex.ext.web.client.HttpResponse;
import io.vertx.reactivex.ext.web.client.WebClient;
import it.unibo.asw.sensor_manager.C;
import it.unibo.asw.sensor_manager.domain.Sensor;
import it.unibo.asw.sensor_manager.util.ObjectIdHelper;

import java.util.*;
import java.util.stream.Collectors;

public class DetectionSimulator extends AbstractVerticle {

    private final Map<Sensor, Long> timerMap;

    private final String host;
    private final int port;

    private WebClient webClient;
    private EventBus eventBus;

    public DetectionSimulator(String host, int port) {
        this.host = host;
        this.port = port;

        timerMap = new HashMap<>();
    }

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        webClient = WebClient.create(this.vertx);
        eventBus = this.vertx.eventBus();
    }

    @Override
    public void start() {
        webClient.get(port, "localhost", "/api/sensors")
                .rxSend()
                .map(HttpResponse::bodyAsJsonArray)
                .map(array -> (List<Object>) array.getList())
                .toObservable()
                .flatMapIterable(x -> x)
                .map(JsonObject::mapFrom)
                .map(Sensor::fromJson)
                .subscribe(this::sample);

        eventBus.<JsonObject>consumer(C.sensors.stored)
                .toObservable()
                .map(Message::body)
                .map(Sensor::fromJson)
                .subscribe(this::sample);

        eventBus.<JsonObject>consumer(C.sensors.destroyed)
                .toObservable()
                .map(Message::body)
                .map(ObjectIdHelper::fromJson)
                .subscribe(opt -> opt.ifPresent(oid -> {
                    timerMap.entrySet().stream()
                            .filter(e -> e.getKey().getId().equals(oid))
                            .map(Map.Entry::getKey)
                            .collect(Collectors.toList())
                            .forEach(sensor -> {
                                System.out.println(sensor);
                                vertx.cancelTimer(timerMap.get(sensor));
                                timerMap.remove(sensor);
                            });
                }));

        eventBus.<JsonObject>consumer(C.rooms.destroyed)
                .toObservable()
                .map(Message::body)
                .map(ObjectIdHelper::fromJson)
                .subscribe(opt -> opt.ifPresent(oid -> {
                    timerMap.entrySet().stream()
                            .filter(e -> e.getKey().getRoom().getId().equals(oid))
                            .map(Map.Entry::getKey)
                            .collect(Collectors.toList())
                            .forEach(sensor -> {
                                vertx.cancelTimer(timerMap.get(sensor));
                                timerMap.remove(sensor);
                            });
                }));
    }

    private void sample(Sensor sensor) {
        final long tid = vertx.setPeriodic(sensor.getSamplingPeriod(), ignore -> {
            final JsonObject request = new JsonObject();
            request.put("sensorId", sensor.getId().toString());
            final JsonArray values = new JsonArray();
            switch (sensor.getType().getName()) {
                case "temperature":
                    values.add(Math.random() * 40.0 - 20.0);
                    break;
                case "presence":
                    values.add(Math.random() >= 0.5);
                    break;
                case "luminosity":
                    values.add(Math.random() * 20.0);
                    break;
                default:
                    values.add(Math.random() * 10 - 5.0);
                    break;
            }
            request.put("values", values);
            System.out.println(request);
            webClient.post(port, host, "/api/detections")
                    .sendJson(request, ar -> {
                        if (ar.succeeded()) {
                            System.out.println("Response received from server");
                        } else {
                            System.err.println("Error while sending request: " + ar.cause().getMessage());
                        }
                    });
        });
        timerMap.put(sensor, tid);
    }

}
