package it.unibo.asw.sensor_manager.api.handlers;

import io.reactivex.disposables.Disposable;
import io.vertx.reactivex.ext.web.RoutingContext;

public interface SensorTypesHandler extends Handler {

    @Override
    Disposable index(RoutingContext context);

}
