package it.unibo.asw.sensor_manager.api;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Handler;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.reactivex.core.AbstractVerticle;
import io.vertx.reactivex.ext.web.Router;
import io.vertx.reactivex.ext.web.RoutingContext;
import io.vertx.reactivex.ext.web.handler.BodyHandler;
import io.vertx.reactivex.ext.web.handler.CorsHandler;
import io.vertx.reactivex.ext.web.handler.StaticHandler;
import io.vertx.reactivex.ext.web.handler.sockjs.SockJSHandler;
import it.unibo.asw.sensor_manager.C;
import it.unibo.asw.sensor_manager.api.handlers.DetectionsHandler;
import it.unibo.asw.sensor_manager.api.handlers.RoomsHandler;
import it.unibo.asw.sensor_manager.api.handlers.SensorTypesHandler;
import it.unibo.asw.sensor_manager.api.handlers.SensorsHandler;
import it.unibo.asw.sensor_manager.simulation.DetectionSimulator;

import java.util.function.Function;

import static io.vertx.core.http.HttpMethod.*;

public class Api extends AbstractVerticle {

    private final DetectionsHandler detectionsHandler;
    private final RoomsHandler roomsHandler;
    private final SensorsHandler sensorsHandler;
    private final SensorTypesHandler sensorTypesHandler;

    private final String host;
    private final int port;

    private final CompositeDisposable disposables = new CompositeDisposable();

    public Api(ApiConfig config) {
        this.detectionsHandler = config.getDetectionsHandler();
        this.roomsHandler = config.getRoomsHandler();
        this.sensorsHandler = config.getSensorsHandler();
        this.sensorTypesHandler = config.getSensorTypesHandler();

        this.host = config.getHost();
        this.port = config.getPort();
    }

    public void start() {
        final Router apiRouter = Router.router(vertx);

        apiRouter.route()
                .handler(BodyHandler.create());

        apiRouter.route().handler(CorsHandler.create("*")
                .allowedMethod(GET)
                .allowedMethod(POST)
                .allowedMethod(PATCH)
                .allowedMethod(PUT)
                .allowedMethod(DELETE)
                .allowedHeader("Access-Control-Allow-Method")
                .allowedHeader("Access-Control-Allow-Origin")
                .allowedHeader("Access-Control-Allow-Credentials")
                .allowedHeader("Content-Type"));

        apiRouter.route("/events/*")
                .handler(sockJSHandler());

        apiRouter.get("/detections")
                .produces("application/json")
                .handler(wrap(detectionsHandler::index));

        apiRouter.post("/detections")
                .consumes("application/json")
                .produces("application/json")
                .handler(wrap(detectionsHandler::store));

        apiRouter.delete("/detections/:id")
                .produces("application/json")
                .handler(wrap(detectionsHandler::destroy));

        apiRouter.get("/rooms")
                .produces("application/json")
                .handler(wrap(roomsHandler::index));

        apiRouter.post("/rooms")
                .consumes("application/json")
                .produces("application/json")
                .handler(wrap(roomsHandler::store));

        apiRouter.route("/rooms/:id")
                .method(PATCH)
                .method(PUT)
                .consumes("application/json")
                .produces("application/json")
                .handler(wrap(roomsHandler::update));

        apiRouter.delete("/rooms/:id")
                .produces("application/json")
                .handler(wrap(roomsHandler::destroy));

        apiRouter.get("/sensors")
                .produces("application/json")
                .handler(wrap(sensorsHandler::index));

        apiRouter.post("/sensors")
                .consumes("application/json")
                .produces("application/json")
                .handler(wrap(sensorsHandler::store));

        apiRouter.get("/sensors/:id")
                .produces("application/json")
                .handler(wrap(sensorsHandler::show));

        apiRouter.route("/sensors/:id")
                .method(PATCH)
                .method(PUT)
                .consumes("application/json")
                .produces("application/json")
                .handler(wrap(sensorsHandler::update));

        apiRouter.delete("/sensors/:id")
                .produces("application/json")
                .handler(wrap(sensorsHandler::destroy));

        apiRouter.get("/sensor-types")
                .produces("application/json")
                .handler(wrap(sensorTypesHandler::index));

        final Router mainRouter = Router.router(vertx);

        mainRouter.mountSubRouter("/api", apiRouter);
        mainRouter.route().handler(StaticHandler.create());

        vertx.createHttpServer()
                .requestHandler(mainRouter::accept)
                .listen(port, ar -> {
                    if (ar.succeeded()) {
                        System.out.println("HTTP server started at http://" + host + ":" + port);
                        vertx.getDelegate().deployVerticle(() -> new DetectionSimulator(host, port), new DeploymentOptions());
                    } else {
                        System.err.println("Could not start HTTP server: " + ar.cause().getMessage());
                    }
                });
    }

    @Override
    public void stop() {
        disposables.clear();
        disposables.dispose();
    }

    private Handler<RoutingContext> wrap(Function<? super RoutingContext, ? extends Disposable> handler) {
        return ctx -> disposables.add(handler.apply(ctx));
    }

    private SockJSHandler sockJSHandler() {
        final BridgeOptions options = new BridgeOptions()
                .addOutboundPermitted(new PermittedOptions().setAddress(C.detections.stored))
                .addOutboundPermitted(new PermittedOptions().setAddress(C.detections.destroyed))
                .addOutboundPermitted(new PermittedOptions().setAddress(C.rooms.stored))
                .addOutboundPermitted(new PermittedOptions().setAddress(C.rooms.updated))
                .addOutboundPermitted(new PermittedOptions().setAddress(C.rooms.destroyed))
                .addOutboundPermitted(new PermittedOptions().setAddress(C.sensors.stored))
                .addOutboundPermitted(new PermittedOptions().setAddress(C.sensors.updated))
                .addOutboundPermitted(new PermittedOptions().setAddress(C.sensors.destroyed));

        return SockJSHandler.create(vertx).bridge(options);
    }

}
