package it.unibo.asw.sensor_manager.util;

public class Utils {

    public static <T> T TODO() {
        return TODO("");
    }

    public static <T> T TODO(String message) {
        throw new UnsupportedOperationException(message);
    }

}
