package it.unibo.asw.sensor_manager.api.handlers.impl;

import io.reactivex.disposables.Disposable;
import io.vertx.core.json.JsonArray;
import io.vertx.reactivex.ext.web.RoutingContext;
import it.unibo.asw.sensor_manager.api.handlers.SensorTypesHandler;
import it.unibo.asw.sensor_manager.domain.SensorType;
import it.unibo.asw.sensor_manager.repositories.SensorTypeRepository;

public class SensorTypesHandlerImpl implements SensorTypesHandler {

    private final SensorTypeRepository repository;

    public SensorTypesHandlerImpl(SensorTypeRepository repository) {
        this.repository = repository;
    }

    @Override
    public Disposable index(RoutingContext context) {
        return repository.findAll()
                .map(SensorType::toJson)
                .toList()
                .map(JsonArray::new)
                .map(Object::toString)
                .subscribe(json -> context.response().end(json));
    }

}
