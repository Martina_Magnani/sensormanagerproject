package it.unibo.asw.sensor_manager.domain;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.asw.sensor_manager.util.JsonHelper;
import it.unibo.asw.sensor_manager.util.ObjectIdHelper;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Room {

    private ObjectId id;
    private String name;

    private List<Sensor> sensors;

    public static Room fromJson(JsonObject json) {
        final Room room = new Room();

        ObjectIdHelper.fromJson(json).ifPresent(room::setId);

        JsonHelper.ifPresentString(json, "name", room::setName);

        JsonHelper.ifPresentJsonArray(json, "sensors", array -> room.setSensors(((List<JsonObject>) array.getList())
                .stream()
                .map(sensorJson -> {
                    sensorJson.remove("roomId");
                    return Sensor.fromJson(sensorJson);
                })
                .collect(Collectors.toList())));

        return room;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    public void setSensors(List<Sensor> sensors) {
        this.sensors = sensors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Room)) return false;
        Room room = (Room) o;
        return Objects.equals(getId(), room.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getName(), getSensors());
    }

    public JsonObject toJson() {
        final JsonObject json = new JsonObject();

        if (id != null) {
            json.mergeIn(ObjectIdHelper.toJson(getId()));
        }

        if (name != null) {
            json.put("name", getName());
        }

        if (sensors != null) {
            json.put("sensors", sensors.stream()
                    .map(s -> {
                        final JsonObject j = s.toJson();
                        j.remove("roomId");
                        return j;
                    })
                    .collect((Supplier<JsonArray>) JsonArray::new, JsonArray::add, JsonArray::add));
        }

        return json;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

}
