package it.unibo.asw.sensor_manager.repositories;

import it.unibo.asw.sensor_manager.domain.Sensor;
import org.bson.types.ObjectId;

public interface SensorRepository extends Repository<Sensor, ObjectId> {
}
