package it.unibo.asw.sensor_manager.repositories.mongo;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.mongo.MongoClient;
import it.unibo.asw.sensor_manager.domain.Detection;
import it.unibo.asw.sensor_manager.repositories.DetectionRepository;
import it.unibo.asw.sensor_manager.util.ObjectIdHelper;
import org.bson.types.ObjectId;

import static it.unibo.asw.sensor_manager.util.Utils.TODO;

public class DetectionRepositoryImpl implements DetectionRepository {

    private final MongoClient client;

    public DetectionRepositoryImpl(MongoClient client) {
        this.client = client;
    }

    @Override
    public Observable<Detection> findAll() {
        return client.rxFind(Collections.DETECTIONS, new JsonObject())
                .toObservable()
                .flatMapIterable(x -> x)
                .flatMap(json -> client.rxFindOne(Collections.SENSORS, new JsonObject()
                        .put("_id", json.getString("sensorId")), null)
                        .map(sensor -> json.put("sensor", sensor))
                        .toObservable())
                .flatMap(json -> client.rxFindOne(Collections.ROOMS, new JsonObject()
                        .put("_id", json.getJsonObject("sensor").getString("roomId")), null)
                        .map(room -> {
                            json.getJsonObject("sensor").put("room", room);
                            return json;
                        })
                        .toObservable())
                .flatMap(json -> client.rxFindOne(Collections.SENSOR_TYPES, new JsonObject()
                            .put("_id", json.getJsonObject("sensor").getString("typeId")), null)
                            .map(type -> {
                                json.getJsonObject("sensor").put("type", type);
                                return json;
                            })
                            .toObservable())
                .map(Detection::fromJson);
    }

    @Override
    public Single<Detection> findById(ObjectId objectId) {
        return TODO("This method should not be called");
    }

    @Override
    public Single<ObjectId> save(Detection detection) {
        final ObjectId id = detection.getId();

        final JsonObject json = detection.toJson();

        json.put("sensorId", detection.getSensor().getId().toString());
        json.remove("sensor");

        return client.rxSave(Collections.DETECTIONS, json)
                .map(newId -> id == null
                        ? ObjectIdHelper.fromString(newId)
                        : id);
    }

    @Override
    public Completable deleteById(ObjectId id) {
        final JsonObject query = ObjectIdHelper.toJson(id);

        return client.rxFindOneAndDelete(Collections.DETECTIONS, query)
                .toCompletable();
    }

}
