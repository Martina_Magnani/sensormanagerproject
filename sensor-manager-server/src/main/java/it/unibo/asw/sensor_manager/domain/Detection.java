package it.unibo.asw.sensor_manager.domain;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.asw.sensor_manager.util.JsonHelper;
import it.unibo.asw.sensor_manager.util.ObjectIdHelper;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Objects;

public class Detection {

    private ObjectId id;
    private String timestamp;

    private Sensor sensor;
    private List<Integer> values;

    public static Detection fromJson(JsonObject json) {
        final Detection detection = new Detection();

        ObjectIdHelper.fromJson(json).ifPresent(detection::setId);

        JsonHelper.ifPresentString(json, "timestamp", detection::setTimestamp);

        JsonHelper.ifPresentJsonArray(json, "values", array -> detection.setValues((List<Integer>) array.getList()));

        JsonHelper.ifPresentJsonObject(json, "sensor", sensor -> detection.setSensor(Sensor.fromJson(sensor)));

        return detection;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
    }

    public List<Integer> getValues() {
        return values;
    }

    public void setValues(List<Integer> values) {
        this.values = values;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Detection)) return false;
        Detection detection = (Detection) o;
        return Objects.equals(getId(), detection.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getTimestamp(), getSensor(), getValues());
    }

    public JsonObject toJson() {
        final JsonObject json = new JsonObject();

        if (id != null) {
            json.mergeIn(ObjectIdHelper.toJson(getId()));
        }

        if (timestamp != null) {
            json.put("timestamp", getTimestamp());
        }

        if (sensor != null) {
            json.put("sensor", getSensor().toJson());
        }

        if (values != null) {
            json.put("values", new JsonArray(getValues()));
        }

        return json;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

}
