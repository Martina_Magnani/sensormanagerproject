package it.unibo.asw.sensor_manager.util;

import io.vertx.core.json.JsonObject;
import org.bson.types.ObjectId;

import java.util.Optional;

public final class ObjectIdHelper {

    public static Optional<ObjectId> fromJson(JsonObject json) {
        if (json != null && json.containsKey("_id")) {
            final Object value = json.getValue("_id");
            if (value != null) {
                if (value instanceof String) {
                    final String oid = (String) value;
                    return Optional.of(fromString(oid));
                } else if (value instanceof JsonObject) {
                    final JsonObject oid = (JsonObject) value;
                    if (oid.containsKey("$oid")) {
                        final String hexString = oid.getString("$oid");
                        return Optional.of(fromString(hexString));
                    }
                }
            }
        }
        return Optional.empty();
    }

    public static ObjectId fromString(String oid) {
        return new ObjectId(oid);
    }

    public static JsonObject toJson(ObjectId id) {
        return new JsonObject().put("_id", id.toString());
    }

    private ObjectIdHelper() {
    }

}
