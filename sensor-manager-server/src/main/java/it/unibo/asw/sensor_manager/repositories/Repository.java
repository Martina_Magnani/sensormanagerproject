package it.unibo.asw.sensor_manager.repositories;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;

public interface Repository<T, ID> {

    Observable<T> findAll();

    Single<T> findById(ID id);

    Single<ID> save(T t);

    Completable deleteById(ID id);

}
