package it.unibo.asw.sensor_manager.api.handlers.impl;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.eventbus.EventBus;
import io.vertx.reactivex.ext.web.RoutingContext;
import it.unibo.asw.sensor_manager.C;
import it.unibo.asw.sensor_manager.api.handlers.SensorsHandler;
import it.unibo.asw.sensor_manager.domain.Sensor;
import it.unibo.asw.sensor_manager.repositories.RoomRepository;
import it.unibo.asw.sensor_manager.repositories.SensorRepository;
import it.unibo.asw.sensor_manager.repositories.SensorTypeRepository;
import it.unibo.asw.sensor_manager.util.ObjectIdHelper;
import org.bson.types.ObjectId;

public class SensorsHandlerImpl implements SensorsHandler {

    private final RoomRepository roomRepository;
    private final SensorRepository sensorRepository;
    private final SensorTypeRepository sensorTypeRepository;

    private final EventBus eventBus;

    public SensorsHandlerImpl(RoomRepository roomRepository, SensorRepository sensorRepository, SensorTypeRepository sensorTypeRepository, EventBus eventBus) {
        this.sensorRepository = sensorRepository;
        this.sensorTypeRepository = sensorTypeRepository;
        this.roomRepository = roomRepository;
        this.eventBus = eventBus;
    }

    @Override
    public Disposable index(RoutingContext context) {
        return sensorRepository.findAll()
                .map(Sensor::toJson)
                .toList()
                .map(JsonArray::new)
                .map(Object::toString)
                .subscribe(json -> context.response().end(json));
    }

    @Override
    public Disposable store(RoutingContext context) {
        final JsonObject body = context.getBodyAsJson();
        final Sensor sensor = Sensor.fromJson(body);

        return roomRepository.findById(ObjectIdHelper.fromString(body.getString("roomId")))
                .map(room -> {
                    sensor.setRoom(room);
                    return sensor;
                })
                .flatMap(s -> sensorTypeRepository.findById(ObjectIdHelper.fromString(body.getString("typeId")))
                .map(type -> {
                    sensor.setType(type);
                    return sensor;
                }))
                .flatMap(sensorRepository::save)
                .map(ObjectIdHelper::toJson)
                .map(json -> sensor.toJson().mergeIn(json))
                .subscribe(json -> {
                    eventBus.publish(C.sensors.stored, json);
                    context.response().end();
                });
    }

    @Override
    public Disposable show(RoutingContext context) {
        final String id = context.request().getParam("id");
        final ObjectId oid = new ObjectId(id);

        return sensorRepository.findById(oid)
                .map(Sensor::toJson)
                .map(Object::toString)
                .subscribe((Consumer<String>) context.response()::end);
    }

    @Override
    public Disposable update(RoutingContext context) {
        final String id = context.request().getParam("id");
        final JsonObject body = context.getBodyAsJson().put("_id", id);

        return sensorRepository.findById(ObjectIdHelper.fromString(id))
                .map(sensor -> {
                    sensor.setName(body.getString("name"));
                    return sensor;
                })
                .flatMap(sensor -> sensorRepository.save(sensor)
                        .map(oid -> sensor.toJson()))
                .subscribe(json -> {
                    eventBus.publish(C.sensors.updated, json);
                    context.response().end();
                });
    }

    @Override
    public Disposable destroy(RoutingContext context) {
        final String id = context.request().getParam("id");
        final ObjectId oid = new ObjectId(id);

        return sensorRepository.deleteById(oid)
                .subscribe(() -> {
                    final JsonObject json = new JsonObject().put("_id", id);
                    eventBus.publish(C.sensors.destroyed, json);
                    context.response().end();
                });
    }

}
