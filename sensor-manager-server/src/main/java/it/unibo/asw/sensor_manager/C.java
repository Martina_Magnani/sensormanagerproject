package it.unibo.asw.sensor_manager;

public final class C {

    public static final class detections {
        private static final String PREFIX = "detections.";

        public static final String stored = PREFIX + "stored";
        public static final String destroyed = PREFIX + "destroyed";

        private detections() {
        }
    }

    public static final class rooms {
        private static final String PREFIX = "rooms.";

        public static final String stored = PREFIX + "stored";
        public static final String updated = PREFIX + "updated";
        public static final String destroyed = PREFIX + "destroyed";

        private rooms() {
        }
    }

    public static final class sensors {
        private static final String PREFIX = "sensors.";

        public static final String stored = PREFIX + "stored";
        public static final String updated = PREFIX + "updated";
        public static final String destroyed = PREFIX + "destroyed";

        private sensors() {
        }
    }

    private C() {
    }

}
