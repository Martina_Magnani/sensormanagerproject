package it.unibo.asw.sensor_manager.domain;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import it.unibo.asw.sensor_manager.util.JsonHelper;
import it.unibo.asw.sensor_manager.util.ObjectIdHelper;
import org.bson.types.ObjectId;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public class Sensor {

    private ObjectId id;
    private String name;
    private long samplingPeriod;

    private int barcodeId;

    private Room room;
    private SensorType type;
    private List<Detection> detections;

    public static Sensor fromJson(JsonObject json) {
        final Sensor sensor = new Sensor();

        ObjectIdHelper.fromJson(json).ifPresent(sensor::setId);

        JsonHelper.ifPresentString(json, "name", sensor::setName);

        JsonHelper.ifPresentLong(json, "samplingPeriod", sensor::setSamplingPeriod);

        JsonHelper.ifPresentInteger(json, "barcodeId", sensor::setBarcodeId);

        JsonHelper.ifPresentJsonObject(json, "room", room -> sensor.setRoom(Room.fromJson(room)));

        JsonHelper.ifPresentJsonObject(json, "type", type -> sensor.setType(SensorType.fromJson(type)));

        JsonHelper.ifPresentJsonArray(json, "detections", array -> sensor.setDetections(((List<Object>) array.getList()).stream()
                .map(JsonObject::mapFrom)
                .map(Detection::fromJson)
                .collect(Collectors.toList())));

        return sensor;
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSamplingPeriod() {
        return samplingPeriod;
    }

    public void setSamplingPeriod(long samplingPeriod) {
        this.samplingPeriod = samplingPeriod;
    }

    public int getBarcodeId() {
        return barcodeId;
    }

    public void setBarcodeId(int barcodeId) {
        this.barcodeId = barcodeId;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public SensorType getType() {
        return type;
    }

    public void setType(SensorType type) {
        this.type = type;
    }

    public List<Detection> getDetections() {
        return detections;
    }

    public void setDetections(List<Detection> detections) {
        this.detections = detections;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Sensor)) return false;
        Sensor sensor = (Sensor) o;
        return Objects.equals(getId(), sensor.getId());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getId(), getName(), getSamplingPeriod(), getBarcodeId(), getRoom(), getType(), getDetections());
    }

    public JsonObject toJson() {
        final JsonObject json = new JsonObject();

        if (id != null) {
            json.mergeIn(ObjectIdHelper.toJson(getId()));
        }

        if (name != null) {
            json.put("name", getName());
        }

        if (room != null) {
            json.put("room", getRoom().toJson());
        }

        if (type != null) {
            json.put("type", getType().toJson());
        }

        if (detections != null) {
            json.put("detections", detections.stream()
                    .map(d -> {
                        final JsonObject j = d.toJson();
                        j.remove("sensorId");
                        return j;
                    })
                    .collect((Supplier<JsonArray>) JsonArray::new, JsonArray::add, JsonArray::add));
        }

        json.put("barcodeId", getBarcodeId());

        return json.put("samplingPeriod", getSamplingPeriod());
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

}
