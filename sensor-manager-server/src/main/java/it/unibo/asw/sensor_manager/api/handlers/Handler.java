package it.unibo.asw.sensor_manager.api.handlers;

import io.reactivex.disposables.Disposable;
import io.vertx.reactivex.ext.web.RoutingContext;

public interface Handler {

    Disposable EMPTY_DISPOSABLE = new Disposable() {
        @Override
        public void dispose() {
        }

        @Override
        public boolean isDisposed() {
            return true;
        }
    };

    default Disposable index(RoutingContext context) {
        return EMPTY_DISPOSABLE;
    }

    default Disposable show(RoutingContext context) {
        return EMPTY_DISPOSABLE;
    }

    default Disposable store(RoutingContext context) {
        return EMPTY_DISPOSABLE;
    }

    default Disposable update(RoutingContext context) {
        return EMPTY_DISPOSABLE;
    }

    default Disposable destroy(RoutingContext context) {
        return EMPTY_DISPOSABLE;
    }

}
