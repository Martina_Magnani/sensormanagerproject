package it.unibo.asw.sensor_manager.repositories.mongo;

final class Collections {

    static final String DETECTIONS = "detections";
    static final String ROOMS = "rooms";
    static final String SENSORS = "sensors";
    static final String SENSOR_TYPES = "sensorTypes";


}
