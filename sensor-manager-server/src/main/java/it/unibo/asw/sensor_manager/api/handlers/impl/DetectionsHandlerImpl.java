package it.unibo.asw.sensor_manager.api.handlers.impl;

import io.reactivex.disposables.Disposable;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.core.eventbus.EventBus;
import io.vertx.reactivex.ext.web.RoutingContext;
import it.unibo.asw.sensor_manager.C;
import it.unibo.asw.sensor_manager.api.handlers.DetectionsHandler;
import it.unibo.asw.sensor_manager.domain.Detection;
import it.unibo.asw.sensor_manager.repositories.DetectionRepository;
import it.unibo.asw.sensor_manager.repositories.RoomRepository;
import it.unibo.asw.sensor_manager.repositories.SensorRepository;
import it.unibo.asw.sensor_manager.repositories.SensorTypeRepository;
import it.unibo.asw.sensor_manager.util.DateUtils;
import it.unibo.asw.sensor_manager.util.ObjectIdHelper;
import org.bson.types.ObjectId;

import java.util.Date;

public class DetectionsHandlerImpl implements DetectionsHandler {

    private final DetectionRepository detectionRepository;
    private final SensorRepository sensorRepository;

    private final EventBus eventBus;

    public DetectionsHandlerImpl(DetectionRepository detectionRepository, RoomRepository roomRepository, SensorRepository sensorRepository, SensorTypeRepository sensorTypeRepository, EventBus eventBus) {
        this.detectionRepository = detectionRepository;
        this.sensorRepository = sensorRepository;
        this.eventBus = eventBus;
    }

    @Override
    public Disposable index(RoutingContext context) {
        return detectionRepository.findAll()
                .map(Detection::toJson)
                .toList()
                .map(JsonArray::new)
                .map(Object::toString)
                .subscribe(json -> context.response().end(json));
    }

    @Override
    public Disposable store(RoutingContext context) {
        final JsonObject body = context.getBodyAsJson();
        final Detection detection = Detection.fromJson(body);

        detection.setTimestamp(DateUtils.toUTCString(new Date()));

        return sensorRepository.findById(ObjectIdHelper.fromString(body.getString("sensorId")))
                .map(sensor -> {
                    detection.setSensor(sensor);
                    return detection;
                })
                .flatMap(detectionRepository::save)
                .map(ObjectIdHelper::toJson)
                .map(json -> detection.toJson().mergeIn(json))
                .subscribe(json -> {
                    eventBus.publish(C.detections.stored, json);
                    context.response().end();
                });
    }

    @Override
    public Disposable destroy(RoutingContext context) {
        final String id = context.request().getParam("id");
        final ObjectId oid = new ObjectId(id);

        return detectionRepository.deleteById(oid)
                .subscribe(() -> {
                    final JsonObject json = new JsonObject().put("_id", id);
                    eventBus.publish(C.detections.destroyed, json);
                    context.response().end();
                });
    }

}
