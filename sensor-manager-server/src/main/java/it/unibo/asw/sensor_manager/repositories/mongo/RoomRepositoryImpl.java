package it.unibo.asw.sensor_manager.repositories.mongo;

import io.reactivex.Completable;
import io.reactivex.Observable;
import io.reactivex.Single;
import io.vertx.core.json.JsonObject;
import io.vertx.reactivex.ext.mongo.MongoClient;
import it.unibo.asw.sensor_manager.domain.Room;
import it.unibo.asw.sensor_manager.repositories.RoomRepository;
import it.unibo.asw.sensor_manager.util.ObjectIdHelper;
import org.bson.types.ObjectId;

public class RoomRepositoryImpl implements RoomRepository {

    private final MongoClient client;

    public RoomRepositoryImpl(MongoClient client) {
        this.client = client;
    }

    @Override
    public Observable<Room> findAll() {
        return client.rxFind(Collections.ROOMS, new JsonObject())
                .toObservable()
                .flatMapIterable(x -> x)
                .flatMap(json -> client.rxFind(Collections.SENSORS, new JsonObject()
                        .put("roomId", json.getString("_id")))
                        .map(sensors -> json.put("sensors", sensors))
                        .toObservable())
                .map(Room::fromJson);
    }

    @Override
    public Single<Room> findById(ObjectId id) {
        final JsonObject query = ObjectIdHelper.toJson(id);

        return client.rxFindOne(Collections.ROOMS, query, null)
                .map(Room::fromJson);
    }

    @Override
    public Single<ObjectId> save(Room room) {
        final ObjectId id = room.getId();

        final JsonObject json = room.toJson();
        json.remove("sensors");
        return client.rxSave(Collections.ROOMS, json)
                .map(newId -> id == null
                        ? ObjectIdHelper.fromString(newId)
                        : id);
    }

    @Override
    public Completable deleteById(ObjectId id) {
        final JsonObject query = ObjectIdHelper.toJson(id);

        return client.rxFind(Collections.SENSORS, new JsonObject().put("roomId", query.getString("_id")))
                .toObservable()
                .flatMapIterable(x -> x)
                .flatMapCompletable(sensor -> client.rxRemoveDocuments(
                        Collections.DETECTIONS,
                        new JsonObject().put("sensorId", sensor.getString("_id")))
                        .toCompletable())
                .andThen(client.rxFindOneAndDelete(Collections.ROOMS, query)
                        .flatMapCompletable(x -> client.rxRemoveDocuments(
                                Collections.SENSORS,
                                new JsonObject().put("roomId", id.toString()))
                                .toCompletable()));
    }

}
