import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { RoomsComponent } from './rooms/rooms.component';
import { SensorDetailsComponent } from './sensor-details/sensor-details.component';
import { ArCameraComponent } from './ar-camera/ar-camera.component';

const routes: Routes = [
  { path: '', component: IndexComponent },
  { path: 'rooms', component: RoomsComponent },
  { path: 'sensor-details/:id', component: SensorDetailsComponent },
  { path: 'ar-camera', component: ArCameraComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
