import { Sensor } from "./sensor";

export interface Room {
    _id: string,
    name: string,
    sensors?: Sensor[]
}
