import { Component, OnInit } from '@angular/core';
import { SensorManagerService } from '../sensor-manager.service';
import { Detection } from '../detection';

import { remove } from 'lodash';

// RxJs imports
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  detections: Detection[];

  private _error = new Subject<void>();
  error = false;

  constructor(private service: SensorManagerService) {
    this.detections = [];
  }

  ngOnInit() {

    this.service.onStoredDetection()
      .subscribe(detection => {
        if (this.detections)
          this.detections.unshift(detection);
      });

    this.service.onDeletedDetection()
      .subscribe(detectionId => remove(this.detections, det => det._id === detectionId._id));

    this._error.pipe(
      debounceTime(5000)
    ).subscribe(() => this.error = false);

  }

}
