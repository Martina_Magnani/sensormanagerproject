import { Injectable } from '@angular/core';
import { Detection } from './detection';
import { Sensor } from './sensor';
import { Room } from './room';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { EventBusService } from './event-bus.service';
import { ObjectId } from './object-id';

@Injectable({
  providedIn: 'root'
})
export class SensorManagerService {

  /* Server REST API Paths*/
  private static SENSORS = '/api/sensors';
  private static ROOMS = '/api/rooms';
  private static DETECTIONS = '/api/detections';

  /* Event Channel Info */
  private static EVENT_CHANNEL_ADDRESS = 'http://sheltered-waters-98453.herokuapp.com/api/events';
  private static STORED_DETECTION = 'detections.stored';
  private static DELETED_DETECTION = 'detections.destroyed';
  private static STORED_SENSOR = 'sensors.stored';
  private static UPDATED_SENSOR = 'sensors.updated';
  private static DELETED_SENSOR = 'sensors.destroyed';
  private static STORED_ROOM = 'rooms.stored';
  private static UPDATED_ROOM = 'rooms.updated';
  private static DELETED_ROOM = 'rooms.destroyed';
  private static STORED_SENSOR_TYPE = 'sensorTypes.stored';
  private static UPDATED_SENSOR_TYPE = 'sensorTypes.updated';
  private static DELETED_SENSOR_TYPE = 'sensorTypes.destroyed';

  /* RxJs Streams */
  private storedDetection: Subject<Detection>;
  private deletedDetection: Subject<ObjectId>;
  private storedRoom: Subject<Room>;
  private updatedRoom: Subject<Room>;
  private deletedRoom: Subject<ObjectId>;
  private updatedSensor: Subject<Sensor>;
  private storedSensor: Subject<Sensor>;
  private deletedSensor: Subject<ObjectId>;

  constructor(private http: HttpClient, eventBus: EventBusService) {
    this.storedDetection = new Subject<Detection>();
    this.deletedDetection = new Subject<ObjectId>();
    this.storedRoom = new Subject<Room>();
    this.updatedRoom = new Subject<Room>();
    this.deletedRoom = new Subject<ObjectId>();
    this.storedSensor = new Subject<Sensor>();
    this.deletedSensor = new Subject<ObjectId>();
    this.updatedSensor = new Subject<Sensor>();

    /* ------------ Vertx Event Bus Management ------------*/
    /* Vertx EventBus Connection */
    eventBus.connect(SensorManagerService.EVENT_CHANNEL_ADDRESS);

    /* Detection Subscriptions */
    eventBus.registerHandler(SensorManagerService.STORED_DETECTION, (err, msg) => {
      this.storedDetection.next(msg.body);
    });

    eventBus.registerHandler(SensorManagerService.DELETED_DETECTION, (err, msg) => {
      this.deletedDetection.next(msg.body);
    });

    /* Sensors Subscriptions */
    eventBus.registerHandler(SensorManagerService.UPDATED_SENSOR, (err, msg) => {
      this.updatedSensor.next(msg.body);
    });

    eventBus.registerHandler(SensorManagerService.STORED_SENSOR, (err, msg) => {
      this.storedSensor.next(msg.body);
    });

    eventBus.registerHandler(SensorManagerService.DELETED_SENSOR, (err, msg) => {
      this.deletedSensor.next(msg.body);
    });

    /* Rooms Subscriptions */
    eventBus.registerHandler(SensorManagerService.STORED_ROOM, (err, msg) => {
      this.storedRoom.next(msg.body);
    });

    eventBus.registerHandler(SensorManagerService.UPDATED_ROOM, (err, msg) => {
      this.updatedRoom.next(msg.body);
    });

    eventBus.registerHandler(SensorManagerService.DELETED_ROOM, (err, msg) => {
      this.deletedRoom.next(msg.body);
    });

  }

  /* --------------- REST API Methods --------------- */
  /* Sensors */
  querySensor(sensorId: string): Observable<Sensor> {
    return this.http.get<Sensor>(SensorManagerService.SENSORS + '/' + sensorId);
  }

  addSensor(sensor: Sensor) {
    return this.http.post<Sensor>(SensorManagerService.SENSORS, sensor).subscribe();
  }

  updateSensor(sensor: Sensor) {
    return this.http.put<Sensor>(SensorManagerService.SENSORS + '/' + sensor._id, sensor).subscribe();
  }

  deleteSensor(sensor: Sensor) {
    return this.http.delete<Sensor>(SensorManagerService.SENSORS + '/' + sensor._id).subscribe();
  }

  /* Rooms */
  queryRooms(): Observable<Room[]> {
    return this.http.get<Room[]>(SensorManagerService.ROOMS);
  }

  queryRoom(room: Room): Observable<Room> {
    return this.http.get<Room>(SensorManagerService.ROOMS + '/' + room._id);
  }

  addRoom(room: Room) {
    return this.http.post<Room>(SensorManagerService.ROOMS, room).subscribe();
  }

  updateRoom(room: Room) {
    return this.http.put<Room>(SensorManagerService.ROOMS + '/' + room._id, room).subscribe();
  }

  deleteRoom(room: Room) {
    return this.http.delete<Room>(SensorManagerService.ROOMS + '/' + room._id).subscribe();
  }

  /* Detections */
  queryDetections(): Observable<Detection[]> {
    return this.http.get<Detection[]>(SensorManagerService.DETECTIONS);
  }

  addDetection(detection: Detection) {
    return this.http.post(SensorManagerService.DETECTIONS, detection).subscribe();
  }

  deleteDetection(detection: Detection) {
    return this.http.delete(SensorManagerService.DETECTIONS + '/' + detection._id).subscribe();
  }


  /* Stream Observables Getters */
  onStoredDetection(): Observable<Detection> {
    return this.storedDetection.asObservable();
  }

  onDeletedDetection(): Observable<ObjectId> {
    return this.deletedDetection.asObservable();
  }

  onStoredRoom(): Observable<Room> {
    return this.storedRoom.asObservable();
  }

  onUpdatedRoom(): Observable<Room> {
    return this.updatedRoom.asObservable();
  }

  onDeletedRoom(): Observable<ObjectId> {
    return this.deletedRoom.asObservable();
  }

  onStoredSensor(): Observable<Sensor> {
    return this.storedSensor.asObservable();
  }

  onUpdatedSensor(): Observable<Sensor> {
    return this.updatedSensor.asObservable();
  }

  onDeletedSensor(): Observable<ObjectId> {
    return this.deletedSensor.asObservable();
  }
}
