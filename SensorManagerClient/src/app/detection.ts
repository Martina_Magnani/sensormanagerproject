import { Sensor } from "./sensor";

export interface Detection {
    _id: string,
    timestamp,
    sensor?: Sensor,
    values?: number[]
}
