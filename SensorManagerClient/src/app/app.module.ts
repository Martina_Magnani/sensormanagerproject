import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { IndexComponent } from './index/index.component';
import { RoomsComponent } from './rooms/rooms.component';
import { SensorDetailsComponent } from './sensor-details/sensor-details.component';
import { AppRoutingModule } from './app-routing.module';
import { SensorManagerService } from './sensor-manager.service';

import { ArCameraComponent } from './ar-camera/ar-camera.component';
import { EventBusService } from './event-bus.service';
import { ServerInterceptor } from './server-interceptor';

@NgModule({
  declarations: [
    AppComponent,
    FooterComponent,
    NavbarComponent,
    IndexComponent,
    RoomsComponent,
    SensorDetailsComponent,
    ArCameraComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    SensorManagerService,
    EventBusService,
    AppComponent,
    { provide: HTTP_INTERCEPTORS, useClass: ServerInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
