import { Component, OnInit } from '@angular/core';
import { SensorManagerService } from '../sensor-manager.service';
import { remove } from 'lodash';
import { Room } from '../room';
import { ObjectId } from '../object-id';

@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {

  rooms: Room[];

  constructor(private service: SensorManagerService) { }

  ngOnInit() {
    this.service.queryRooms()
      .subscribe(rooms => this.rooms = rooms);

    this.service.onStoredRoom()
      .subscribe(room => {
        this.rooms.push(room)
      });

    this.service.onUpdatedRoom()
      .subscribe(room => {
        remove(this.rooms, r => r._id === room._id);
        this.rooms.push(room);
      });

    this.service.onDeletedRoom()
      .subscribe(oid => {
        remove(this.rooms, room => room._id === oid._id);
      });

    this.service.onStoredSensor()
      .subscribe(sensor => {
        this.rooms.find(room => room._id === sensor.room._id).sensors.push(sensor)
      });

    this.service.onUpdatedSensor()
      .subscribe(sensor => {
        var sensorRoom = this.rooms.find(room => room._id === sensor.room._id);
        remove(sensorRoom.sensors, sens => sens._id === sensor._id);
        sensorRoom.sensors.push(sensor);
      });

    this.service.onDeletedSensor()
      .subscribe(sensorId => {
        var sensorRoom = this.findSensorRoom(sensorId);
        remove(sensorRoom.sensors, sensor => sensor._id === sensorId._id);
      });
  }

  findSensorRoom(sensorId: ObjectId): Room {
    for (let room of this.rooms) {
      var sensor = room.sensors.find(sensor => sensor._id === sensorId._id);
      if (sensor) return room;
    }
  }
}
