import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { SensorManagerService } from '../sensor-manager.service';
import { ActivatedRoute } from '@angular/router';
import { Sensor } from '../sensor';
import { Detection } from '../detection';
import { Room } from '../room';
import { remove } from 'lodash';

@Component({
  selector: 'app-sensor-details',
  templateUrl: './sensor-details.component.html',
  styleUrls: ['./sensor-details.component.css']
})
export class SensorDetailsComponent implements OnInit, OnDestroy {

  sensor: Sensor;
  detections: Detection[];
  room: Room;
  private sub: any;

  constructor(private service: SensorManagerService, private route: ActivatedRoute) { }

  ngOnInit() {
    /* Fetch Sensor ID */
    this.sub = this.route.params.subscribe(params => {
      const sensorId = params['id'];

      /* Load sensor data */
      this.service.querySensor(sensorId)
        .subscribe(sensor => {
          this.sensor = sensor;
          this.detections = sensor.detections;
          this.room = sensor.room;
        });

      /* Subscribe for detections changes */
      this.service.onStoredDetection()
        .subscribe(detection => {
          if (detection.sensor._id === sensorId && this.sensor.detections) {
            this.sensor.detections.unshift(detection);
          }
        });

      this.service.onDeletedDetection()
        .subscribe(detectionId => {
          remove(this.detections, detection => detection._id === detectionId._id);
        });

      /* Subscribe for sensor changes*/
      this.service.onUpdatedSensor()
        .subscribe(sensorUpdated => {
          if (sensorUpdated._id === this.sensor._id) {
            this.sensor = sensorUpdated;
          }
        });
    });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
