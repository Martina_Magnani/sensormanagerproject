import { TestBed, inject } from '@angular/core/testing';

import { SensorManagerService } from './sensor-manager.service';

describe('SensorManagerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SensorManagerService]
    });
  });

  it('should be created', inject([SensorManagerService], (service: SensorManagerService) => {
    expect(service).toBeTruthy();
  }));
});
