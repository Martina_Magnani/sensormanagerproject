import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-ar-camera',
  templateUrl: './ar-camera.component.html',
  styleUrls: ['./ar-camera.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ArCameraComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
