import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArCameraComponent } from './ar-camera.component';

describe('ArCameraComponent', () => {
  let component: ArCameraComponent;
  let fixture: ComponentFixture<ArCameraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArCameraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArCameraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
