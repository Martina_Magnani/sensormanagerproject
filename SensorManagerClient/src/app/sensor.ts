import { Room } from "./room";
import { Detection } from "./detection";
import { SensorType } from "./sensor-type";

export interface Sensor {
    _id: string,
    name: string,
    samplingPeriod: number,
    detections?: Detection[],
    room?: Room,
    type?: SensorType
}
