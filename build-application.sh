#!/bin/bash
cd SensorManagerClient
ng build --prod --aot
cd ..
rm -r sensor-manager-server/src/main/resources/webroot/*
mv SensorManagerClient/dist/* sensor-manager-server/src/main/resources/webroot
cd sensor-manager-server
./gradlew stage
cd ..
